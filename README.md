<p align="center">
    <h1 align="center">Visicom Maps Services <i>for</i> PHP</h1>
    <br>
</p>

PHP client library(SDK) for Visicom Maps API Web Services


REQUIREMENTS
------------
This library requires the following:

- PHP 5.4.0+\|7.0+
- guzzlehttp/guzzle 5.3.1+\|6.0+
- Visicom Maps API key 

### API keys

Each Visicom Maps Web Service request requires an API key. API keys
are freely available with a Google Account at
https://api.visicom.ua/accounts/pub/dapi.
INSTALLATION
------------

Run Composer in your project:

    composer require shyna0890/visicom-maps-services
    
Then you could call it after Composer is loaded depended on your PHP framework:

```php
require __DIR__ . '/vendor/autoload.php';

use shyna0890\visicomMaps\Client;
```

---

USAGE
-----

Before using any Visicom Maps Services, first you need to create a Client with configuration, then use the client to access Visicom Maps Services.

### Client

Create a Client using [API key]((#api-keys)):

```php
$vmaps = new \shyna0890\visicomMaps\Client(['key'=>'Your API Key']);
```

#### Language

You could set language for Client for all services:

```php
$vmaps = new \shyna0890\visicomMaps\Client(['key'=>'Your API Key', 'lang'=>'ua']);
```

Changing language during execution:

```php
$vmaps->setLanguage('en');
// ...
```

### Directions API

```php
// Request directions via public transit
$directionsResult = $vmaps->directions('Raisy Okipnoi Street', 'Kiev', [
    'departure_time' => time(),
    ]);
```

### Geocoding API

```php
// Geocoding an address
$geocodeResult = $vmaps->geocode('Raisy Okipnoi Street Kiev');

// Look up an address with reverse geocoding
$reverseGeocodeResult = $vmaps->reverseGeocode([ 30.59355321604309,50.4499695032737]);
```



