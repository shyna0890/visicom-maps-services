<?php

namespace shyna0890\visicomMaps;

use Exception;
use GuzzleHttp\Client as HttpClient;

/**
 * Visicom Maps PHP Client
 *
 * @author  Vasya Shinkaruk <shyna0890@gmail.com>
 * @version 1.0.0
 *
 * @method array directions(string $origin, string $destination, array $params = [])
 * @method array geocode(string $address, array $params = [])
 * @method array reverseGeocode(string $latlng, array $params = [])
 */
class Client
{
    /**
     * Google Maps Platform base API host
     */
    const API_HOST = 'https://api.visicom.ua';

    /**
     * For service autoload
     *
     * @see http://php.net/manual/en/language.namespaces.rules.php
     */
    const SERVICE_NAMESPACE = "\\shyna0890\\visicomMaps\\";

    /**
     * For Client-Service API method director
     *
     * @var array Method => Service Class name
     */
    protected static $serviceMethodMap
        = [
            'directions'     => 'Directions',
            'geocode'        => 'Geocoding',
            'reverseGeocode' => 'Geocoding',
        ];

    /**
     * Visicom API Key
     *
     * Authenticating by API Key, otherwise by client ID/digital signature
     *
     * @var string
     */
    protected $apiKey;

    /**
     * GuzzleHttp\Client
     *
     * @var GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * Visicom Maps default language
     *
     * @var string ex. 'uk'
     */
    protected $language;

    /**
     * Constructor
     *
     * @param string|array $optParams API Key or option parameters
     *  'key' => Visicom API Key
     * @return self
     */
    function __construct($optParams)
    {
        // Quick setting for API Key
        if (is_string($optParams)) {
            // Params as a string key
            $key              = $optParams;
            $optParams        = [];
            $optParams['key'] = $key;
        }

        // Assignment
        $key         = isset($optParams['key']) ? $optParams['key'] : null;
        $defaultLang = isset($optParams['language']) ? $optParams['language'] : null;

        // Use API Key
        if ($key) {
            $this->apiKey = (string)$key;
        } else {
            throw new Exception("Unable to set Client credential due to your wrong params", 400);
        }

        // Default Language setting
        if ($defaultLang) {
            $this->setLanguage($defaultLang);
        }

        // Load GuzzleHttp\Client
        $this->httpClient = new HttpClient([
            'base_uri' => self::API_HOST,
            'timeout'  => 5.0,
        ]);

        return $this;
    }

    /**
     * Request Visicom Map API
     *
     * @param string $apiPath
     * @param array $params
     * @param string $method HTTP request method
     * @param string $body
     * @return GuzzleHttp\Psr7\Response
     */
    public function request($apiPath, $params = [], $method = 'GET', $body = null)
    {

        // Guzzle request options
        $options = [
            'http_errors' => false,
        ];
        // Parameters for Auth
        $defaultParams = ['key' => $this->apiKey];
        // Parameters for Language setting
        if ($this->language) {
            $defaultParams['lang'] = $this->language;
        }

        // Query
        $options['query'] = array_merge($defaultParams, $params);

        // Body
        if ($body) {
            $options['body'] = $body;
        }
        return $this->httpClient->request($method, $apiPath, $options);
    }

    /**
     * Set default language for Visicom Maps API
     *
     * @param string $language ex. 'zh-TW'
     * @return self
     */
    public function setLanguage($language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Client methods refer to each service
     *
     * All service methods from Client calling would leave out the first argument (Client itself).
     *
     * @param string Client's method name
     * @param array Method arguments
     * @return mixed Current service method return
     */
    public function __call($method, $arguments)
    {
        // Matching self::$serviceMethodMap is required
        if (!isset(self::$serviceMethodMap[$method]))
            throw new Exception("Call to undefined method " . __CLASS__ . "::{$method}()", 400);

        // Get the service mapped by method
        $service = self::$serviceMethodMap[$method];

        // Fill Client in front of arguments
        array_unshift($arguments, $this);

        return call_user_func_array([self::SERVICE_NAMESPACE . $service, $method], $arguments);
    }
}
