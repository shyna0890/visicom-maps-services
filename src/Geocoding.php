<?php

namespace shyna0890\visicomMaps;

use shyna0890\visicomMaps\Service;
use shyna0890\visicomMaps\Client;

/**
 * Geocoding Service
 *
 * @author  Vasya Shinkaruk <shyna0890@gmail.com>
 * @since   1.0.0
 * @see https://api.visicom.ua/uk/products/data-api/data-api-references/geocode
 */
class Geocoding extends Service
{
    const API_PATH = '/data-api/5.0/uk/geocode.json';

    /**
     * Reverse Geocode
     *
     * @param Client $client
     * @param string $address
     * @param array Query parameters
     * @return array Result
     */
    public static function geocode(Client $client, $address = null, $params = [])
    {
        if (is_string($address))
            $params['text'] = $address;

        return self::requestHandler($client, self::API_PATH, $params);
    }

    /**
     * Reverse Geocode
     *
     * @param Client $client
     * @param array|string $latlng ['lat', 'lng'] or place_id string
     * @param array Query parameters
     * @return array Result
     */
    public static function reverseGeocode(Client $client, $latlng, $params = [])
    {
        if (is_string($latlng)) {

            $params['near'] = $latlng;

        } else {

            list($lat, $lng) = $latlng;
            $params['near'] = "{$lat},{$lng}";
        }

        return self::requestHandler($client, self::API_PATH, $params);
    }
}
