<?php

namespace shyna0890\visicomMaps;

use shyna0890\visicomMaps\Service;
use shyna0890\visicomMaps\Client;

/**
 * Directions Service
 * 
 * @author  Vasya Shinkaruk <shyna0890@gmail.com>
 * @since   1.0.0
 * @see     https://api.visicom.ua/uk/products/data-api/data-api-references/distance
 */
class Directions extends Service
{
    const API_PATH = '/data-api/5.0/core/distance.json';

    /**
     * Directions
     *
     * @param Client $client
     * @param string $origin 
     * @param string $destination 
     * @param array Query parameters
     * @return array Result
     */
    public static function directions(Client $client, $origin, $destination, $params=[])
    {
        $params['origin'] = (string) $origin;
        $params['destination'] = (string) $destination;

        return self::requestHandler($client, self::API_PATH, $params);
    }
}
