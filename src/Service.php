<?php

namespace shyna0890\visicomMaps;

/**
 * Visicom Maps Abstract Service
 * 
 * @author  Vasya Shinkaruk <shyna0890@gmail.com>
 */
abstract class Service 
{
    /**
     * Define by each service
     * 
     * @param string
     */
    const API_PATH = '';
    
    /**
     * Request Handler
     *
     * @param Client $client
     * @param string $apiPath
     * @param array $params
     * @param string $method HTTP request method
     * @return array|mixed Formated result
     */
    protected static function requestHandler(Client $client, $apiPath, $params, $method='GET')
    {
        $response = $client->request($apiPath, $params, $method);
        $result = $response->getBody()->getContents();
        $result = json_decode($result, true);

        // Error Handler
        if (200 != $response->getStatusCode())
            return $result;
        elseif (isset($result['error_message']))
            return $result;

        return  isset($result['results']) ? $result['results'] : $result;
    }
}
